import { Role, Status, User } from "@/domain/user";

const result: User[] = [
  {
    id: 1,
    email: "norman.osborn@oscorp.com",
    firstName: "Norman",
    lastName: "Osborn",
    role: Role.ADMIN,
    status: Status.ACTIVE,
    photo:
      "https://vignette.wikia.nocookie.net/marveldatabase/images/5/50/Norman_Osborn_%28Earth-1048%29_from_Marvel%27s_Spider-Man_%28video_game%29_001.jpg/revision/latest/scale-to-width-down/350?cb=20180925092853"
  },
  {
    id: 2,
    email: "harry.osborn@oscorp.com",
    firstName: "Harry",
    lastName: "Osborn",
    role: Role.ADMIN,
    status: Status.INACTIVE,
    photo:
      "https://vignette.wikia.nocookie.net/marveldatabase/images/d/de/Harry_Osborn_%28Earth-1048%29_from_Marvel%27s_Spider-Man_%28video_game%29_0001.png/revision/latest?cb=20181008230302"
  },
  {
    id: 3,
    email: "peter.parker@gmail.com",
    firstName: "Peter",
    lastName: "Parker",
    role: Role.DOCTOR,
    status: Status.ACTIVE,
    photo:
      "https://static.tvtropes.org/pmwiki/pub/images/mcu_spider_man_ffh.png"
  },
  {
    id: 4,
    email: "maryjane@gmail.com",
    firstName: "Mary Jane",
    lastName: "Watson",
    role: Role.DOCTOR,
    status: Status.ACTIVE,
    photo:
      "https://vignette.wikia.nocookie.net/marveldatabase/images/3/38/Mary_Jane_Watson_%28Earth-1048%29_from_Marvel%27s_Spider-Man_%28video_game%29_001.jpg/revision/latest?cb=20180922183118"
  },
  {
    id: 5,
    email: "miles.morales@gmail.com",
    firstName: "Miles",
    lastName: "Morales",
    role: Role.ACCOUNTANT,
    status: Status.ACTIVE,
    photo:
      "https://vignette.wikia.nocookie.net/marveldatabase/images/f/f7/Miles_Morales_%28Earth-1048%29_from_Marvel%27s_Spider-Man_%28video_game%29_002.png/revision/latest/scale-to-width-down/350?cb=20181229060016"
  }
];

export class UserService {
  public listAll() {
    //  Let's simulate some async network load.
    return new Promise<User[]>(resolve => {
      const networkTime = Math.random() * 1000;

      setTimeout(() => resolve(result), networkTime);
    });
  }
}

export const userService = new UserService();
