import Vue from "vue";
import Router from "vue-router";
import Root from "@/views/root.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "root",
      component: Root
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./views/login.vue")
    },
    {
      path: "/users",
      name: "users",
      component: () => import("./views/users.vue")
    }
  ]
});

export default router;
