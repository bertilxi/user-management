export enum Role {
  DOCTOR = "DOCTOR",
  ADMIN = "ADMIN",
  ACCOUNTANT = "ACCOUNTANT"
}

export enum Status {
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE"
}

export interface User {
  id: number | undefined;
  email: string;
  firstName: string;
  lastName: string;
  role: Role;
  status: Status;
  photo?: string;
}
