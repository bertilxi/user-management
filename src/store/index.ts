import Vue from "vue";
import Vuex from "vuex";
import { State, state } from "@/store/state";
import * as mutations from "./mutations";
import * as actions from "./actions";

Vue.use(Vuex);

const store = new Vuex.Store<State>({
  state,
  mutations,
  actions
});

export default store;
