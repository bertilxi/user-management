import { userService } from "@/services/user";
import { Action } from "vuex";
import { Status, User } from "@/domain/user";
import { State } from "@/store/state";

export const login: Action<State, State> = (context, user: User) => {
  context.commit("SET_USER", user);
};

export const fetchUsers: Action<State, State> = context => {
  userService.listAll().then(users => {
    context.commit("SET_USERS", users);
  });
};

export const editUser: Action<State, State> = (context, user: User) => {
  context.commit("SET_EDITING_USER", user);
};

export const saveUser: Action<State, State> = (context, editedUser: User) => {
  const users = [...context.state.users];

  if (typeof editedUser.id === "number") {
    const index = users.findIndex(user => user.id === editedUser.id);
    users[index] = editedUser;
  } else {
    editedUser.id = users.length + 1;
    editedUser.status = Status.INACTIVE;
    users.push(editedUser);
  }

  context.commit("SET_USERS", users);
  context.commit("SET_EDITING_USER", undefined);
};
