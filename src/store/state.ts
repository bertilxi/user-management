import { User } from "@/domain/user";

export interface State {
  user?: User;
  users: User[];
  editingUser?: User;
}

export const state: State = {
  user: undefined,
  users: [],
  editingUser: undefined
};
