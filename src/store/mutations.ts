import { State } from "@/store/state";
import { User } from "@/domain/user";

export function SET_USER(state: State, user: User) {
  state.user = user;
}

export function SET_USERS(state: State, users: User[]) {
  state.users = users;
}

export function SET_EDITING_USER(state: State, user: User) {
  state.editingUser = user;
}
