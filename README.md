# Scratchpay code challenge

This is just a basic implementation of a SPA scaffolded with the Vue CLI.

### The app consist in 3 routes:
- The root page (we can say it is abstract) for handling authentication as a route guard.
- The login page
- The user management page

### Packages used
- Vue as UI lib
- Vue Router as router
- Vuex as reactive flux like store
- Spectre css for css classes and helpers
- mdi font for icons
- Muli typeface for fonts

### Features considered for improvement
- Unit, integration and E2E Test.
- Better and stricter types.
- Documentation for the code that could be not as readable as I thought.
- Using LocalStorage/IndexedDB for store the credentials properly.
- Handling errors gracefully (notifications for local errors, and a 404 page for routing errors).
- Adding a loading indicator for network loads.
- Adding animations for route transitions and component entering and exiting.
- Cleaner and smoother css styles.
- Using all the colors of the palette (it was difficult to use every color without overloading the page, IMO).
- Improving the responsiveness, the user detail component could have a better ux for mobile devices.
- There are some typing errors that could be fixed quickly.

### My development process

#### Day 1 (Tuesday 6 of August):
It was late, I received the code challenge email, I read the assignment quickly and left it for later.

#### Day 2 (Wednesday 7 of August):
I read the challenge in detail, many times, and started to think the best and simpler way to solve it. I designed a simple domain for the user, roles and statuses, and left the rest for later.
After work, At night I scaffolded a simple Vue project, and added some dependencies, a css library, the requested font-face and the color variables, and mock it up a quick layout for the user list.

#### Day 3 (Thursday 8 of August):
I looked at Dribbble for some inspiration (steal like an artist, right?). 

#### Day 4 (Friday 9 of August):
It was a really busy day, I couldn't continue with it.

#### Day 5 (Saturday 10 of August):
This was the first time that I could take the project being rested and quiet.
I cleaned it up a little what I did, added a login, the user creation and edition component, some validations, and I wrote this.

### Disclaimer
It was a really busy week at my current job, was difficult to take time after work to tackle this, I really wanted to finish this as soon as possible.

Also I found difficult not continue working on this improving it, I personally don't like how basic it is, but given the time it is ok I guess.

The goal of quality instead of quantity made me think more than I would like, I took a lot of time during the days thinking what would be the best for accomplish this in such a little time.

I really enjoyed question myself about what to do and how.